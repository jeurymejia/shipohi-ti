# Group Auth

## Authenticate [POST /api/v1/api-token-auth/]

+ Attributes
    + username - string (required)
    + password - string (required)


+ Request (application/json)

        {
            "username": "admin",
            "password": "admin"
        }

+ Response 200 (application/json)

        {
            "token": "f0c26c1db4baa6a42d16dc0d48f76f693adcd5db"
        }