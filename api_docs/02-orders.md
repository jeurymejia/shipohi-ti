# Group Orders

## Place ORDER [POST /api/v1/orders/]

+ Attributes
    + address1 - string (required)
    + city - string (required)
    + province_code - string (required)
    + zip_code - string (required)
    + item_count - int 
    + item_price - int
    + total_price - int

+ Headers
    + Authorization: Token **tokenid**

+ Request (application/json)

        {
            "item_count": 10,
            "item_price": 50,
            "total_price": 500,
            "address1": "10 Park Ave",
            "city": "Newark",
            "province_code": "NJ",
            "zip_code": "07105"
        }

+ Response 201 (applicatio/json)

        {
            "warehouse": {
                "id": 2,
                "name": "New Jersey HQ",
                "address1": "17 Pone Street",
                "city": "Newark",
                "province_code": "NJ",
                "country_code": "US",
                "zip_code": "07105",
                "created_at": "2020-02-12T04:33:17.015125Z",
                "updated_at": "2020-02-12T04:33:17.015162Z"
            },
            "total_price": 500,
            "item_count": 10,
            "item_price": 50,
            "address1": "10 Park Ave",
            "city": "Newark",
            "province_code": "NJ",
            "country_code": "US",
            "zip_code": "07104"
        }