FROM python:3.6

MAINTAINER Jeury Mejia

ADD . /usr/src/app
WORKDIR /usr/src/app
COPY requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt
ENV DB_ENGINE=django.db.backends.postgresql