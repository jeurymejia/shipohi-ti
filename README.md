# Shipohi Technical Interview ~ Solution
>Solution to Ohi's own Technical Interview.


# Requirements
This application makes use of [Docker](https://docs.docker.com/docker-for-mac/install/) containers + [docker-compose](https://docs.docker.com/compose/install/). To setup and run the application, docker must be present in the system and it must be **running!**

# Setup and run
Docker compose:

```
$ docker-compose up
```

This will start the set of services needed to run the app. The application will be running on **`http://0.0.0.0:8000`**

# Order API
API to place an ORDER. Docs found [here](https://bitbucket.org/jeurymejia/shipohi-ti/src/master/api_docs/02-orders.md).

## Test Accounts
Use any of the following test accounts for authentication and to use this API:

| Username | Password|
|-----------|--------|
| admin  | admin |
| nick  | admin |
| jeurymejia  | admin |

## Authentication
Most calls to the API will require an authorization token. After logging in, you will use this token to authorize subsequent calls.

#### Token Authentication

**Method**

| URI                               | HTTP Method | Authentication |
|-----------------------------------|-------------|----------------|
| http://0.0.0.0:8000/api/api-token-auth/ | POST        |	None           |

**Fields**

| Parameter | Type   | Description   | Default | Required |
|-----------|--------|---------------|---------|----------|
| username  | String | Your username | N/A     | **Yes**  |
| password  | String | Your password  | N/A     | **Yes**  |

**Request sample**

```
curl -X POST \
  http://0.0.0.0:8000/api/api-token-auth/
  -H 'Content-Type: application/json'
  -d '{"username":"admin","password":"admin"}'
```

**Response**

| Key   | Type   |Description|
|-------|--------|-----------|
| token | String | The authorization token you must pass to calls which require authentication |

**Response sample**

```
{
  "token": "582238650a728a585a9aef0da7a17aca0ae8567b"
}
```

## Place order

**Method**

| URI                               | HTTP Method | Authentication |
|-----------------------------------|-------------|----------------|
| http://0.0.0.0:8000/api/v1/orders/ | POST        |	Token           |


**Request sample**

```
curl -X POST
  http://0.0.0.0:8000/api/v1/orders/
  -H 'Authorization: Token 582238650a728a585a9aef0da7a17aca0ae8567b'
  -H 'Content-Type: application/json'
  -d '{"item_count": 10, "item_price": 50, "total_price": 500, "address1": "10 Park Ave", "city": "Newark", "province_code": "NJ", "zip_code": "07104"}'
```

**Response sample**

```
{
    "warehouse": {
        "id": 2,
        "name": "New Jersey HQ",
        "address1": "17 Pone Street",
        "city": "Newark",
        "province_code": "NJ",
        "country_code": "US",
        "zip_code": "07105",
        "created_at": "2020-02-12T04:33:17.015125Z",
        "updated_at": "2020-02-12T04:33:17.015162Z"
    },
    "total_price": 0,
    "item_count": 10,
    "item_price": 50,
    "address1": "8 Park Ave",
    "city": "Newark",
    "province_code": "NJ",
    "country_code": "US",
    "zip_code": "07104"
}
```