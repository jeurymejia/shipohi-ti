from django.core.management.base import BaseCommand
from api.models import Warehouse

class Command(BaseCommand):
    help = 'Use this command to populate the database with Warehouse data'

    def handle(self, *args, **options):
        warehouses = [
            {
                'name': 'New York HQ',
                'address1': '17 Pine Street',
                'city': 'New York',
                'province_code': 'NY',
                'country_code': 'US',
                'zip_code': '10001'
            },
            {
                'name': 'New Jersey HQ',
                'address1': '17 Pone Street',
                'city': 'Newark',
                'province_code': 'NJ',
                'country_code': 'US',
                'zip_code': '07105'
            },
            {
                'name': 'San Francisco HQ',
                'address1': '17 Pane Street',
                'city': 'San Francisco',
                'province_code': 'CA',
                'country_code': 'US',
                'zip_code': '94016'
            }
        ]

        for warehouse in warehouses:
            Warehouse.objects.get_or_create(**warehouse)

        print("Built warehouses...")