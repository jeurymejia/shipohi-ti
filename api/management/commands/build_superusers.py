from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

class Command(BaseCommand):
    help = 'Use this command to build top level superusers'

    def _build_super_users(self):
        print("Building superusers...")

        test_users = ["admin", "nick", "jeurymejia"]

        for user in test_users:
            obj = User.objects.create_superuser(
                username=user,
                email="{}@shipohi.com".format(user),
                password='admin')

            print("Built account for {}".format(user))

    def handle(self, *args, **options):
        self._build_super_users()
