from rest_framework import serializers
from api.models import Order, Warehouse

class WarehouseSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Warehouse
        fields = '__all__'


class OrderSerializer(serializers.ModelSerializer):

    warehouse =  WarehouseSerializer(required=False)
    total_price = serializers.IntegerField(required=False)
    item_count = serializers.IntegerField(required=False)
    item_price = serializers.IntegerField(required=False)

    class Meta:
        model = Order
        fields = ['warehouse', 'total_price', 'item_count', 'item_price',
                  'address1', 'city', 'province_code', 'country_code', 'zip_code']