from django.db import models
from geopy.geocoders import Nominatim
from googlemaps import Client
from utility.distance import calc_dist

class Warehouse(models.Model):
    """Barebones warehouse model"""
    name = models.CharField(max_length=255)
    address1 = models.CharField(max_length=255)
    city = models.CharField(max_length=255)
    province_code = models.CharField(max_length=255)
    country_code = models.CharField(max_length=255, default="US")
    zip_code = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @staticmethod
    def get_closest_warehouse(cls, **kwargs):
        gmaps = Client('AIzaSyCDOo5B16eTco4EcLdbsbkKvzom64ndsJs')
        order_address = f"{kwargs['address1']} {kwargs['city']} {kwargs['province_code']}, {kwargs.get('country_code', 'US')} {kwargs['zip_code']}"
        order_geo = gmaps.geocode(order_address)[0].get("geometry").get("location")
        order_lat, order_lng = order_geo.get("lat"), order_geo.get("lng")
                
        closest_warehouse, smallest_dist = None, float('inf')

        for w in cls.objects.all():
            warehouse_address = f"{w.address1} {w.city} {w.province_code}, {w.country_code} {w.zip_code}"
            warehouse_geo = gmaps.geocode(warehouse_address)[0].get("geometry").get("location")
            warehouse_lat, warehouse_lng = warehouse_geo.get("lat"), warehouse_geo.get("lng")
            order_to_warehouse_distance = calc_dist(
                warehouse_lat, 
                warehouse_lng, 
                order_lat, 
                order_lng)

            if order_to_warehouse_distance < smallest_dist:
                smallest_dist = order_to_warehouse_distance
                closest_warehouse = w.id

        return closest_warehouse


class Order(models.Model):
    """Oversimplified single-item order model"""
    warehouse = models.ForeignKey(Warehouse, on_delete=models.CASCADE)
    address1 = models.CharField(max_length=255)
    city = models.CharField(max_length=255)
    province_code = models.CharField(max_length=255)
    country_code = models.CharField(max_length=255, default="US")
    zip_code = models.CharField(max_length=255)
    item_count = models.IntegerField(default=0)
    item_price = models.IntegerField(default=0)
    total_price = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)